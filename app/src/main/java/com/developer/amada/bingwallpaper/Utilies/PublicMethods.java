package com.developer.amada.bingwallpaper.Utilies;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.developer.amada.bingwallpaper.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class PublicMethods {

    /**
     * for show Toast in all project
     * @param mContext
     * @param str
     */
    public static void showToast(Context mContext, String str) {

        Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
    }

    /**
     * for Show Image In View
     * @param mContext
     * @param path
     * @param imageView
     */
    public static void setImageOnView(Context mContext, String path, ImageView imageView){

        Picasso.with(mContext).load(path).fit().centerCrop().into(imageView);
    }

    /**
     * this function for
     * @param nameFolder
     * @return
     */
    public static boolean createFolderInPictureDir(String nameFolder) {

        File folder = new File(Environment.DIRECTORY_PICTURES + File.separator + nameFolder);

        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }

        return success;

    }

}
