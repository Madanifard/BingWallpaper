package com.developer.amada.bingwallpaper.BingWallpaper;

import com.developer.amada.bingwallpaper.BingWallpaper.BingApi.BingApi;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BingApiInterface {

    @GET("http://www.bing.com/HPImageArchive.aspx")
    Call<BingApi> getImage(@Query("format") String format, @Query("idx") int idx, @Query("n") int n, @Query("mkt") String mkt);
}
