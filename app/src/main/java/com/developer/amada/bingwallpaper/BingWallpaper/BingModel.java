package com.developer.amada.bingwallpaper.BingWallpaper;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.developer.amada.bingwallpaper.BingWallpaper.BingApi.BingApi;
import com.developer.amada.bingwallpaper.Utilies.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BingModel {

    BingContract.presenter presenter;

    public BingModel(BingContract.presenter presenter){
        this.presenter = presenter;
    }

    public void getBingImage() {

//        try {
//            BingApi bingApi = Constants.bingApiInterface.getImage("js", 0, 1, "en-Us").execute().body();
//            presenter.getResponseBingApi(bingApi);
//        } catch (IOException e) {
//            e.printStackTrace();
//            presenter.getResponseErrorBingApi();
//        }
        Constants.bingApiInterface.getImage("js", 0, 1, "en-Us").enqueue(new Callback<BingApi>() {
            @Override
            public void onResponse(Call<BingApi> call, Response<BingApi> response) {
                presenter.getResponseBingApi(response.body());
            }

            @Override
            public void onFailure(Call<BingApi> call, Throwable t) {
                presenter.getResponseErrorBingApi();
            }
        });
    }

    /**
     * for Downloading the image
     * @param mContext
     * @param url
     * @return
     */
    public String downloadBingImage(Context mContext, String url) {

        final String[] filePath = {"not Download"};

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(mContext) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Log.d("AsyncHttpClient", "onFailure: ");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {

                Log.d("AsyncHttpClient", "onSuccess: ");
                filePath[0] = file.getAbsolutePath();

            }
        });

        return filePath[0];
    }
}
