package com.developer.amada.bingwallpaper.BingWallpaper;

import android.content.Context;

import com.developer.amada.bingwallpaper.BingWallpaper.BingApi.BingApi;

public interface BingContract {

    interface View {

        void showWelcome(String str);

        void showErrorApi(String string);

        void getResponseBingApi(String imageUrl, String pathUrlSaveImage);

    }

    interface presenter {

         void attachView(View view);
         void attachContext(Context mContext);

         void callBingApi();

         void getResponseErrorBingApi();

         void getResponseBingApi(BingApi bingApi);

    }

}
