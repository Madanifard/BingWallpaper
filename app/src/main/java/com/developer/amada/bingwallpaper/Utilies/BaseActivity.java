package com.developer.amada.bingwallpaper.Utilies;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    public Context mContext = this;
    public Activity mActivity = this;

    public String DebugTag = "debug";
}
