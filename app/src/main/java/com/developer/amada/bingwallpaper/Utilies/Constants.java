package com.developer.amada.bingwallpaper.Utilies;

import com.developer.amada.bingwallpaper.BingWallpaper.BingApiInterface;

public class Constants {

    public static String BaseUrl = "http://www.bing.com/HPImageArchive.aspx/";

    public static BingApiInterface bingApiInterface = RetrofitGenerator.createBingService(BingApiInterface.class);
}
