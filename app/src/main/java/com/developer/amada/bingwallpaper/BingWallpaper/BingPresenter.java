package com.developer.amada.bingwallpaper.BingWallpaper;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.developer.amada.bingwallpaper.BingWallpaper.BingApi.BingApi;
import com.developer.amada.bingwallpaper.BingWallpaper.BingApi.Image;

public class BingPresenter implements BingContract.presenter {


    private BingContract.View view;
    BingModel model = new BingModel(this);
    private Context mContext;

    @Override
    public void attachView(BingContract.View view) {

        this.view = view;

        //view.showWelcome("Hello Amirreza");

    }

    @Override
    public void attachContext(Context mContext) {

        this.mContext = mContext;
    }

    @Override
    public void callBingApi() {

        model.getBingImage();
    }

    @Override
    public void getResponseErrorBingApi() {

        view.showErrorApi("Error In Call Bing API");
    }

    @Override
    public void getResponseBingApi(BingApi bingApi) {

        String BingBaseUrl = "https://www.bing.com";
        String imageUrl = "";
        String pathDownloadImage = "";

        for (Image image:
                bingApi.getImages()) {

            imageUrl = image.getUrl();

        }

        model.downloadBingImage(mContext, BingBaseUrl + imageUrl);

        view.getResponseBingApi(BingBaseUrl + imageUrl, pathDownloadImage);
    }


}
