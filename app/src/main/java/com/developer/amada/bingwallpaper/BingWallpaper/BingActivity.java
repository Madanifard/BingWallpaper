package com.developer.amada.bingwallpaper.BingWallpaper;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.developer.amada.bingwallpaper.Utilies.BaseActivity;
import com.developer.amada.bingwallpaper.R;
import com.developer.amada.bingwallpaper.Utilies.PublicMethods;

public class BingActivity extends BaseActivity implements BingContract.View {

    BingContract.presenter presenter;
    ProgressDialog progressDialog;
    ImageView bingImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bing);

        progressDialog = new ProgressDialog(mContext);

        bindWidget();

        initPresenter();
    }

    /**
     * for bind the widget
     */
    public void bindWidget(){

        bingImage = findViewById(R.id.bing_image);
    }

    /**
     * connect View to Presenter
     */
    public void initPresenter() {

        presenter = new BingPresenter();
        presenter.attachView(this);
        presenter.attachContext(mContext);

    }

    /**
     * OnClickLisener for Button
     * @param view
     */
    public void callBingApi(View view){

        progressDialog.setTitle("Waiting for get Data");
        progressDialog.show();
        presenter.callBingApi();
    }

    @Override
    public void showWelcome(String str) {

        PublicMethods.showToast(mContext, str);
    }

    @Override
    public void showErrorApi(String string) {

        progressDialog.hide();
        PublicMethods.showToast(mContext, string);
    }

    @Override
    public void getResponseBingApi(String imageUrl, String pathDownloadImage) {

        PublicMethods.setImageOnView(mContext, imageUrl, bingImage);
        PublicMethods.showToast(mContext, pathDownloadImage);
        progressDialog.hide();

        //get permission for Write in SD Card
//        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(mActivity,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    1500);
//            PublicMethods.downloadFile();
//        } else {
//
//            PublicMethods.showToast(mContext, "Cannot Save Image In Folder");
//        }

    }


}
